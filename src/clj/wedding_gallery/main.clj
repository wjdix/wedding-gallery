(ns wedding-gallery.main
  (:gen-class)
  (:require [wedding-gallery.system :as system]
            [com.stuartsierra.component :as component]))

(defn -main [& args]
  (let [env (System/getenv)
        port (Integer/parseInt (get env "PORT" "8080"))
        s3-secret-key (get env "S3_SECRET_KEY")
        s3-access-key (get env "S3_ACCESS_KEY")
        s3-bucket-name (get env "S3_BUCKET_NAME")
        s3-prefix (get env "S3_PREFIX")
        password (get env "BASIC_AUTH_PASSWORD")]
    (component/start
     (system/system {:port port
                     :s3-secret-key s3-secret-key
                     :s3-access-key s3-access-key
                     :s3-bucket-name s3-bucket-name
                     :password password
                     :s3-prefix s3-prefix}))))
