(ns wedding-gallery.system
  (:require [com.stuartsierra.component :as component]
            [wedding-gallery.bucket :refer [create-bucket]]
            [wedding-gallery.web.server :refer [create-server]]
            [wedding-gallery.web.handler :refer [create-handler]]))

(defn system [config-options]
  (component/system-map
   :bucket (create-bucket config-options)
   :app (component/using
         (create-handler config-options)
         [:bucket])
   :jetty (component/using
           (create-server config-options)
           [:app])))

