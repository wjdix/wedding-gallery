(ns wedding-gallery.bucket
  (:require [com.stuartsierra.component :as component]
            [clj-time.core :as t]
            [aws.sdk.s3 :as s3]))

(defrecord Bucket [cred bucket-name prefix]
  component/Lifecycle
  (start [this] this)
  (stop [this]
    (dissoc this :cred)))

(defn create-bucket [{secret-key :s3-secret-key access-key :s3-access-key bucket-name :s3-bucket-name prefix :s3-prefix }]
  (map->Bucket {:cred {:secret-key secret-key :access-key access-key} :bucket-name bucket-name :prefix prefix}))

(defn list-images [{cred :cred bucket-name :bucket-name prefix :prefix}]
  (:objects (s3/list-objects cred bucket-name {:prefix prefix})))

(defn present-images [{cred :cred bucket-name :bucket-name} {key :key}]
  {:url (s3/generate-presigned-url cred bucket-name key {:expires (t/date-time 2015 12 12)})
   :name key})
