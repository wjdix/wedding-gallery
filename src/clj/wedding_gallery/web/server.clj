(ns wedding-gallery.web.server
  (:require [com.stuartsierra.component :as component]
            [ring.adapter.jetty :refer [run-jetty]]))

(defrecord Server [port server app]
  component/Lifecycle
  (start [this]
    (let [server (run-jetty app {:join? false :port port})]
      (assoc this :server server)))
  (stop [this]
    (when server 
      (.stop server)
      this)))

(defn create-server [{port :port app :app}]
  (map->Server {:port port :app app}))
