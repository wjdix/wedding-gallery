(ns wedding-gallery.web.handler
  (:require [compojure.core :refer [defroutes GET routes]]
            [compojure.handler :as handler]
            [compojure.route :as route]
            [ring.middleware.basic-authentication :refer [wrap-basic-authentication]]
            [ring.util.response :refer [file-response]]
            [com.stuartsierra.component :as component]
            [ring.middleware.logger :refer [wrap-with-logger]]
            [wedding-gallery.web.actions :refer [list-images]]))

(defn authenticated? [expected-user expected-password provided-user provided-password]
  (and (= expected-user provided-user)
       (= expected-password provided-password)))

(defn secured-routes [bucket password]
  (-> (routes
       (GET "/" _ (file-response "public/html/index.html" {:root "resources"}))
       (GET "/images" _ (fn [_] ((partial list-images bucket)))))
      (wrap-basic-authentication (partial authenticated? "GUEST" password))))

(defn build-routes [bucket password]
  (routes
   (secured-routes bucket password)
   (route/files "/" {:root "resources/public"})))

(defrecord Handler [bucket password]
  component/Lifecycle
  (start [this]
    (-> (handler/site (build-routes bucket password))
        wrap-with-logger))
  (stop [this]))

(defn create-handler [{bucket :bucket password :password}]
  (map->Handler {:bucket bucket :password password}))



