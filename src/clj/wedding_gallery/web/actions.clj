(ns wedding-gallery.web.actions
  (:require [wedding-gallery.bucket :as bucket-ops]))

(defn generate-response [data & [status]]
  {:status (or status 200)
   :headers {"Content-Type" "application/edn"}
   :body (pr-str data)})

(defn list-images [bucket]
  (generate-response (vec (map (partial bucket-ops/present-images bucket) (bucket-ops/list-images bucket)))))
