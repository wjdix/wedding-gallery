(ns wedding-gallery.core
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [cljs.reader :as reader]
            [goog.events :as events]
            [goog.dom :as gdom]
            [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [cljs.core.async :refer [put! chan <!]])
  (:import [goog.net XhrIo]
           goog.net.EventType
           [goog.events EventType]))

(enable-console-print!)

(def ^:private meths
  {:get "GET"})

(defn edn-xhr [{:keys [method url data on-complete]}]
  (let [xhr (XhrIo.)]
    (events/listen xhr goog.net.EventType.COMPLETE
                   (fn [e]
                     (on-complete (reader/read-string (.getResponseText xhr)))))
    (. xhr
       (send url (meths method) (when data (pr-str data))
             #js {"Content-Type" "application/edn"}))))

(def app-state
  (atom {:current-index 0
         :images []}))

(defn displayable [data owner _]
  (reify
    om/IInitState
    (init-state [_])
    om/IRenderState
    (render-state [_ _]
      (let [url (get data :url)
            filename (get data :name)]
        (dom/span nil
                (dom/img 
                 #js {:src url
                      :style #js {:width "800px"}
                      :alt filename}))))))
(defn listen [el type]
  (let [out (chan)]
    (events/listen el type
                   (fn [e] (put! out e)))
    out))

(defn bound [num lower upper]
  (max lower (min num upper)))

(defn images-view [app owner]
  (reify
    om/IWillMount
    (will-mount [_]
      (edn-xhr 
       {:method :get
        :url "images"
        :on-complete #(om/transact! app :images (fn [_] %))})
      (let [keypresses (listen (gdom/getWindow) "keyup")]
        (go (while true
              (let [key-event (<! keypresses)
                    char-code (.-keyCode key-event)]
                (case char-code
                  37 (om/transact! app :current-index (fn [x] (max (dec x) 0))) 
                  39 (om/transact! app :current-index (fn [x] (inc x)))
                  (println char-code)))))))
    om/IRender
    (render [_]
      (println (:images app))
      (println (:current-index app))
      (dom/div #js {:id "images"}
               (dom/h2 nil "Images")
               (let [bounded-index (bound (:current-index app) 0 (count (:images app)))
                     image-to-display (get (:images app) (:current-index app))]
                 (println image-to-display)
                 (om/build displayable image-to-display))))))

(om/root images-view
         app-state
         {:target (gdom/getElement "images")})

