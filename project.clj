(defproject wedding-gallery "0.1.0-SNAPSHOT"
  :description "TODO"
  :url "TODO"
  :license {:name "TODO: Choose a license"
            :url "http://choosealicense.com/"}
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [clj-aws-s3 "0.3.10"]
                 [compojure "1.3.1"]
                 [ring "1.2.1"]
                 [ring.middleware.logger "0.5.0"]
                 [com.stuartsierra/component "0.2.2"]
                 
                 [ring-basic-authentication "1.0.5"]

                 [org.clojure/clojurescript "0.0-2665"]
                 [org.om/om "0.8.0"]
                 [org.clojure/core.async "0.1.346.0-17112a-alpha"]]
  :source-paths ["src/clj" "src/cljs"]
  :hooks [leiningen.cljsbuild]
  :plugins [[lein-cljsbuild "1.0.4"]]
  :cljsbuild {
              :builds {
                       :production {:source-paths ["src/cljs"]
                                    :compiler {
                                               :output-to "resources/public/js/main.js"
                                               :output-dir "resources/public/js/out-foo"
                                               :optimizations :advanced
                                               :pretty-print false
                                               }}
                       :dev {:source-paths ["src/cljs"]
                             :compiler {
                                        :output-to "resources/public/js/main.js"
                                        :output-dir "resources/public/js/out"
                                        :optimizations :whitespace
                                        :pretty-print true }}
                       }
              }
  :min-lein-version "2.0.0"
  :uberjar-name "wedding-gallery-standalone.jar"
  :profiles {:dev {:dependencies [[org.clojure/tools.namespace "0.2.7"]]
                   :source-paths ["dev"]}
             :uberjar {:main wedding-gallery.main :aot :all}})
